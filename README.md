# ComputerVision-2024-25 Consumer demo repo

The url of the created netorca instance is https://api-university.demo.netorca.io/v1/

The material in this repository will be used for the  Computer Vision course held in 2024-2025


- User as Director of the Course: `ProfPretto`, password: `NetorcaUniDemo1`
- User as TA: `phd_student1`,      password: `NetorcaUniDemo1`
- User as student: `student1`,      password: `NetorcaUniDemo1`
- User as student: `student2`,      password: `NetorcaUniDemo1`


University of Padova
Department of Computer Engineering
Faculty and student office
